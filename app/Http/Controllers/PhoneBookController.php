<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\PhoneNum;
use App\services\PhoneNumeStoreHelper;

class PhoneBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['contacts'] = Contact::all();
        return view('phonebook', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $phoneBook = new Contact;

        $phoneBook->name = $request->contact_name;
        $phoneBook->save();

        foreach ($request->phonenums as $key=>$number){
            $phoneNumber = new PhoneNum;
            $phoneNumber->contact_id = $phoneBook->id;
            $phoneNumber->phone_num = $number;
            $phoneNumber->save();
        }

       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $phoneBook
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $phoneBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $phoneBook
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $phoneBook, $id)
    {
        $contact = Contact::find($id);
        return view('edit', ['contact'=>$contact]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $phoneBook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::find($id);
        $contact->name = $request->name;
        $contact->save();
        $phoneNumUpdate = new PhoneNumeStoreHelper();

        $phoneNumUpdate->phoneNumsSync($request, $id);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $phoneBook
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return redirect()->back();
    }
}
