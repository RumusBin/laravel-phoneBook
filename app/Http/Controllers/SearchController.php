<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\PhoneNum;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $result = [];
        $term = $request->term;
        $phoneNums = PhoneNum::where('phone_num', 'LIKE', '%'.$term.'%')->get();
        $contacts = Contact::where('name', 'LIKE', '%'.$term.'%')->get();
        if(count($contacts)>0){
            foreach ($contacts as $contact){
                $result = [$contact->name];
            }
        }
        if (count($phoneNums)>0){
            foreach ($phoneNums as $num){
                $result = ["$num->phone_num"];
            }
        }
        if(count($phoneNums) ===0 && count($contacts)===0){
            $result = ['Not matches for this request!'];
        }
        return $result;
    }
}
