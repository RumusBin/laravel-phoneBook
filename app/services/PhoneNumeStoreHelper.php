<?php

namespace App\services;
use App\Models\PhoneNum;

class phoneNumeStoreHelper
{
    public function phoneNumsSync($request, $id)
    {
        $phoneNums = PhoneNum::where('contact_id', $id)->get();
        foreach ($phoneNums as $nums){
            $nubers = PhoneNum::find($nums->id);

            $nubers->delete();
        }
        foreach ($request->phonenums as $key=>$number){
            $phoneNumber = new PhoneNum;
            $phoneNumber->contact_id = $id;
            $phoneNumber->phone_num = $number;
            $phoneNumber->save();
        }
    }

}