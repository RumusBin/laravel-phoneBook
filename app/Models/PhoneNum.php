<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneNum extends Model
{
    protected $table = 'phoneNums';
    protected $fillable = ['phone_num'];

    public function contact()
    {
        return $this->belongsTo('App\Models\Contact');
    }
}
