@extends('layouts.main')
@section('content')
    <nav class="panel column is-offset-2 is-8">
        <form method="post" action="{{route('phonebook.update', $contact->id)}}">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <p class="panel-heading">
                Contact {{$contact->name}} edit
            </p>

            <div class="field">
                <label class="label">Name</label>
                <div class="control has-icons-left has-icons-right">
                    <input class="input" name="name" type="text" placeholder="Text input" value="{{$contact->name}}">
                    <span class="icon is-small is-left">
                     <i class="fa fa-user"></i>
                </span>
                </div>
            </div>

            @foreach($contact->phoneNums as $num)
                <div class="field">
                    <label class="label">Phone number</label>
                    <div class="control panel-block has-icons-left has-icons-right ">
                        <input class="input phone_field column is-10" name="phonenums[]" type="text" value="{{$num->phone_num}}">
                        <span class="icon is-small is-left">
                            <i class="fa fa-phone"></i>
                        </span>
                        <button class="panel-icon item-delete column is-2">
                            <i class="has-text-danger fa fa-trash"></i>
                        </button>
                    </div>
                </div>
            @endforeach

            <div class="new-phone-nums"></div>
            <div id="num_add"><img class="add_phone_num"></div>
            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link">Submit</button>
                </div>
                <div class="control">
                    <button class="button is-text">Cancel</button>
                </div>
            </div>
        </form>
    </nav>
@endsection