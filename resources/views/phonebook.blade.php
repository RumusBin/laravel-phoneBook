@extends('layouts.main')
@section('content')
    <nav class="panel column is-offset-2 is-8">
        <p class="panel-heading">
            Phone book
            <button id="add_contact" class="button is-link is-outlined">
                Add contact
            </button>
        </p>
        <div class="panel-block">
            <p class="column is-7"></p>
            <p class="control has-icons-left column is-5">
                <input class="input is-small" type="text" id="searchContact" name="searchContact" placeholder="search">

            </p>
        </div>

        @if($contacts->count()>0)
            @foreach($contacts as $contact)

        <div class="panel-block">
            <span class="column is-5">
                {{$contact->name}}
            </span>
            <span class="column is-5">
                <p>Phones: </p>
                <div class="select">
                  <select>
                      @foreach($contact->phoneNums as $num)
                    <option>{{$num->phone_num}}</option>
                      @endforeach
                  </select>
                </div>
            </span>
            <a href="{{route('phonebook.edit', $contact->id)}}"  class="panel-icon column is-1">
                    <i class="has-text-info fa fa-edit"></i>
            </a>
            <a href="" class="panel-icon column is-1"
               title="Delete this post" onclick="
                    if(confirm('Are you sure you want to delete this contact?'))
                    {
                    event.preventDefault(); document.getElementById('delete-form-{{$contact->id}}').submit()
                    }">
                <i class="has-text-danger fa fa-trash"></i>
            </a>
            <form action="{{route('phonebook.destroy', $contact->id)}}" method="post" id="delete-form-{{$contact->id}}" style="display: none">
                {{csrf_field()}}
                {{method_field('DELETE')}}
            </form>
        </div>
            @endforeach
         @endif
    </nav>
    <div class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">Add new contact</p>
                <button id="close_modal" class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <form action="/phonebook" method="post" id="new_contact_form">
                    {{csrf_field()}}
                    <div class="field">
                        <label class="label">Name</label>
                        <div class="control">
                            <input class="input contact_name" type="text" name="contact_name" placeholder="Contact name" required>
                            <span id="name_error"></span>
                        </div>
                            <p class="help">Enter contact name</p>
                    </div>
                    <div class="field">
                        <label class="label">Phone</label>
                        <div class="control">
                            <input class="input phone_field contact_phone" type="text" name="phonenums[]" placeholder="Phone number" required>
                            <span id="phone_error"></span>
                        </div>
                        <p class="help">Enter contact phone number</p>
                    </div>
                    <div class="new-phone-nums"></div>
                </form>
                <div id="num_add"><img class="add_phone_num"></div>
            </section>
            <footer class="modal-card-foot">
                <button class="button is-success" id="new_contact_success">Save contact</button>
                <button id="cancel" class="button">Cancel</button>
            </footer>
        </div>
    </div>

@stop