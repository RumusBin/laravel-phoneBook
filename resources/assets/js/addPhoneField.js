let new_phone_field = '<div class="field">'+'<label class="label">'+'Phone Number'+'</label>'+'<div class="control panel-block">'+
    '<input class="input phone_field contact_phone column is-10"  type="text" placeholder="Enter new phone number"  name="phonenums[]" required>'+
    '<span id="phone_error"></span>'+
    '<button class="panel-icon item-delete column is-2">'+'<i class="has-text-danger fa fa-trash"></i>'+
    '</button>'+'</div>'+'</div>';
$('#num_add').on('click', function () {
    let field_nums = $('.phone_field').length;
    $('.new-phone-nums').append(new_phone_field).text();
    field_nums += 1;
    console.log(field_nums);
    if(field_nums===10){
        $('#num_add').fadeOut("slow");
    }
});
